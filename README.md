# Coco DLL Bootstrap
The Coco DLL Bootstrap repository was originally where the DLL of Coco was originally hosted, these days, the Coco DLL Bootstrap repository is home to most of Coco Z's online operations. Coco Z uses the Coco DLL Bootstrap repository to download it's DLL, check if Coco is updated, check if the computer is compatible with One-Click Login, send messages written by Coco Technology, and show and update Coco Z's EULA.

# Why Gitlab instead of Github?
C# based programs do not treat Github very well, as Github suffers from long text updating times, unlike Github, Gitlab's text update times are almost instant, which is essential for our operations, as we can keep things up to date, instantly.

# Is anything malicious being done?
Nothing malicious is being done here, Coco only uses the internet to check things here, and to execute scripts from the CCControl repository. We can assure you that Coco Z is not a virus or a R.A.T Trojan, and any detections from the program are bound to be false.